﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TcpChatServer
{
    class Program
    {
        static Server server;
        static Thread listenThread;

        static void Main(string[] args)
        {
            try
            {
                server = new Server();
                listenThread = new Thread(new ThreadStart(server.Listen));//задаем задачу прослушивать сервер, обращаясь к объекту сервера
                listenThread.Start();//старт потока
            }

            catch(Exception ex)
            {
                server.Disconnect();
                Console.WriteLine(ex.Message);
            }
        }
    }
}
