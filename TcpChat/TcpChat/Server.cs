﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace TcpChatServer
{
   public class Server
    {
        static TcpListener tcpListener;//наш сервер
        List<Client> clientsList = new List<Client>();//список активных пользователей чата

        protected internal void AddConnection(Client client)//метод для добавление клиентов в список клиентов
        {
            clientsList.Add(client);
        }

        protected internal void RemoveClientConnection(string id)
        {
            Client client = clientsList.FirstOrDefault(c => c.Id == id);//возращает элемент по найденому айдишнику

            if(client!=null)//если данный клиент существует, то удаляем его из списка клиентов
            {
                clientsList.Remove(client);
            }

        }

        protected internal void Listen()//начинаем прослушку подключений
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, 8888);//объявляем объект для прослушивания и присваиваем ему порт 8888 + любой доступный компу айпишник
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true) //обработка клиентов и потоков
                {
                    TcpClient Tcpclient = tcpListener.AcceptTcpClient();

                    Client client = new Client(Tcpclient, this);
                    Thread clientThread = new Thread(new ThreadStart(client.Process));
                    clientThread.Start();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Disconnect();
            }
        }
        protected internal void BroadcastMessage(string message, string id)//передача сообщения одного клиента всем другим клиентам
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            for(int i =0; i<clientsList.Count; i++)
            {
                if(clientsList[i].Id!=id)//если это не тот клиент, который отправил сообщение, то выполняем отправку
                {
                    clientsList[i].Stream.Write(data, 0, data.Length);//отправка сообщения
                }
            }
        }

        protected internal void Disconnect()
        {
            tcpListener.Stop();//остановка работы сервера

            for(int i =0; i<clientsList.Count; i++)
            {
                clientsList[i].Close();//отключаем всех клиентов из списка
            }
            Environment.Exit(0);//завершение процесса

        }
    }
}
