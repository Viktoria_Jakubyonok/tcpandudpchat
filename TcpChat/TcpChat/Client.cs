﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace TcpChatServer
{
   public class Client
    {
       protected internal string Id { get; private set; }//можно обратиться к этому члену в этом классе и в производном(который может находиться в другой сборке)
       protected internal NetworkStream Stream { get; private set; }//поток чтения и передачи данных
       string userName;//имя пользователя
       TcpClient client;//клиент для передачи данных серверу
       Server server;//объект сервера

       public Client(TcpClient tcpClient, Server serverT)
       {
           Id = Guid.NewGuid().ToString();//задает почти УНИКАЛЬНОЕ значение
           client = tcpClient;
           server = serverT;
           server.AddConnection(this); //установка соединения с заданным сервером
       }

       private string GetMessage()//чтение байтов и преобразования их в строку на экране
       {
           byte[] data = new byte[256];
           StringBuilder builder = new StringBuilder();
           int bytes = 0;
           do
           {
               bytes = Stream.Read(data, 0, data.Length);
               builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
           }
           while (Stream.DataAvailable);

           return builder.ToString();
       }
       public void Process()
       {
           try
           {
               Stream = client.GetStream();//иницилизируем поток для работы с данными клиента

               string message = GetMessage();////???????
               userName = message;

               message = userName + " вошел в чат";
               server.BroadcastMessage(message, this.Id);//посылаем сообщение всем пользователям о входе в чат
               Console.WriteLine(message);

               while(true)//получаем сообщения всех клиентов 
               {
                   try
                   {
                       message = GetMessage();
                       message = String.Format("{0}: {1}", userName, message);
                       Console.WriteLine(message);
                       server.BroadcastMessage(message, this.Id);
                   }
                   catch
                   {
                       message = String.Format("{0}: покинул чат", userName);
                       Console.WriteLine(message);
                       server.BroadcastMessage(message, this.Id);
                       break;
                   }
               }
           }

           catch(Exception e)
           {
               Console.WriteLine(e.Message);
           }

           finally
           {
               server.RemoveClientConnection(this.Id);//если что-то не так и мы вышли из цикда, то закрываем ресурсы
               Close();
           }
       }
       protected internal void Close()
       {
           if (Stream != null)
               Stream.Close();
           if (client != null)
               client.Close();
       }
           
       }
}
