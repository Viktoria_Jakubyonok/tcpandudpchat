﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace receiver
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient clientReceiver = new UdpClient(1111);//указываем порт,котрый будет принимать сообщения

            IPEndPoint remoteIPEndPoint = null;//для всех адресов, пытающих отослать сообщение

            try
            {
                while(true)
                {
                    byte[] receiveBytes = clientReceiver.Receive(ref remoteIPEndPoint);
                    string returnDate = Encoding.Unicode.GetString(receiveBytes);
                    Console.WriteLine("Входящее сообщение: {0}, адрес входящего подключения: {1}", returnDate, remoteIPEndPoint.ToString());

                    //отправляем ответ на сообщение
                    byte[] sendBytes = Encoding.Unicode.GetBytes("Сообщение дошло до сервера");
                    clientReceiver.Send(sendBytes, sendBytes.Length, remoteIPEndPoint);
                }
            }

            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                clientReceiver.Close();
            }

        }
    }
}
