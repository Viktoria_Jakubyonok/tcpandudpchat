﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace sender
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient clientSender = new UdpClient();

            IPAddress ipAddr = IPAddress.Parse("127.0.0.1");
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 1111);
            Console.WriteLine("Введите сообщение: ");

            
                try
                {
                    while (true)
                    {
                        //информация задается в конструкторе
                        // UdpClient clientSender = new UdpClient(ipEndPoint);

                        //информация задается в методе Connect();
                        // clientSender.Connect(ipEndPoint);
                        string message = Console.ReadLine();
                        byte[] sendBytes = Encoding.Unicode.GetBytes(message);
                        clientSender.Send(sendBytes, sendBytes.Length, ipEndPoint);

                        byte[] receiveBytes = clientSender.Receive(ref ipEndPoint);
                        string returnDate = Encoding.Unicode.GetString(receiveBytes);
                        Console.WriteLine(returnDate);
                    }

                    //clientSender.Close();
                }


                catch (Exception ex)
                {
                    Console.WriteLine("Ошибка при подключении:" + ex.ToString());
                }
            finally
                {
                    clientSender.Close();
                }
            


        }
    }
}
